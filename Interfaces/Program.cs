﻿using Interfaces.Testability;
using System;
using System.Collections.Generic;
using Interfaces.Exercises;
using Interfaces.Extensibility;
using Interfaces.Polymorphism;

namespace Interfaces
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var workflowEngine = new WorkflowEngine();
            var workflow = new Workflow();
            workflow.Add(new UploadVideo());
            workflow.Add(new ExternalVideoEncoder());
            workflow.Add(new MailService());
            workflow.Add(new ProcessVideo());
            workflowEngine.Run(workflow);
        }

        private static void Polymorphism()
        {
            var encode = new VideoEncoder();
            encode.RegisterNotificationChannel(new SmsNotificationChannel());
            encode.RegisterNotificationChannel(new MailNotificationChannel());
            encode.Encode(new Video());
        }

        private static void Extensibility()
        {
            var migration = new DbMigration(new FileLogger());
            migration.Migrate();
        }

        private static void OrderProcessing()
        {
            var orderProcessor = new OrderProcessor(new ShippingCalculator());
            var order = new Order()
            {
                DatePlaced = DateTime.Now,
                TotalPrice = 20f
            };

            orderProcessor.Process(order);
            Console.WriteLine(order);

            orderProcessor.Process(order);
            Console.WriteLine(order);
        }
    }
}
