﻿namespace Interfaces.Exercises
{
    public interface IActivity
    {
        void Execute();
    }
}