﻿using System.Collections.Generic;

namespace Interfaces.Exercises
{
    public class Workflow : IWorkflow
    {
        private readonly IList<IActivity> _activities;

        public Workflow()
        {
            _activities = new List<IActivity>();
        }

        public void Add(IActivity activity) => _activities.Add(activity);

        public void Remove(IActivity activity) => _activities.Remove(activity);
        public IEnumerable<IActivity> GetActivities() => _activities;
    }
}