﻿using System;

namespace Interfaces.Exercises
{
    public class MailService : IActivity
    {
        public void Execute()
        {
            Console.WriteLine("Send an email to the owner of the video notifying them that the video started processing.");
        }
    }
}