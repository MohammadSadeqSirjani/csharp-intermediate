﻿using System.Text;

namespace Interfaces.Exercises
{
    public class WorkflowEngine
    {
        public void Run(IWorkflow workflow)
        {
            foreach (var activity in workflow.GetActivities()) activity.Execute();
        }
    }
}
