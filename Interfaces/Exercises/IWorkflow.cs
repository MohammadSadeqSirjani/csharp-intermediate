﻿using System.Collections.Generic;

namespace Interfaces.Exercises
{
    public interface IWorkflow
    {
        void Add(IActivity activity);

        void Remove(IActivity activity);

         IEnumerable<IActivity> GetActivities();
    }
}