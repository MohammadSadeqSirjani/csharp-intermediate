﻿using System;

namespace Interfaces.Exercises
{
    public class UploadVideo : IActivity
    {
        public void Execute() => Console.WriteLine("Uploading video to a cloud storage.");
    }
}