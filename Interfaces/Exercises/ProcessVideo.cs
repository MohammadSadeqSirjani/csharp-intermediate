﻿using System;

namespace Interfaces.Exercises
{
    public class ProcessVideo : IActivity
    {
        public void Execute() => Console.WriteLine("Change the status of the video record in the database to “Processing”.");
    }
}