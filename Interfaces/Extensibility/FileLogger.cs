﻿using System;
using System.IO;

namespace Interfaces.Extensibility
{
    public class FileLogger : ILogger
    {
        private readonly string _path = $"{Timestamp.GetTimestamp()}.txt";
        private const string Root = "../../../Extensibility/File";

        public FileLogger() => _path = Path.Combine(AppContext.BaseDirectory, Root, _path);

        public void LogError(string message) => Log("Error", message);

        public void LogInfo(string message) => Log("Info", message);

        public void LogWarning(string message) => Log("Warning", message);

        public void LogNotice(string message) => Log("Notice", message);

        private void Log(string messageType, string message)
        {
            using var writer = new StreamWriter(_path, true);
            writer.WriteLine($"{messageType}: {message}");

        }
    }
}