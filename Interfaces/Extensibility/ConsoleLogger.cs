﻿using System;
using System.Threading;

namespace Interfaces.Extensibility
{
    public class ConsoleLogger : ILogger
    {
        public void LogError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
            Sleep();
        }

        public void LogInfo(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ResetColor();
            Sleep();
        }

        public void LogWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ResetColor();
            Sleep();
        }

        public void LogNotice(string message)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(message);
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.ResetColor();
            Sleep();
        }

        private static void Sleep(int speed = 100) => Thread.Sleep(speed);
    }
}