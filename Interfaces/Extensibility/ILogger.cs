﻿namespace Interfaces.Extensibility
{
    public interface ILogger
    {
        void LogError(string message);
        void LogInfo(string message);
        void LogWarning(string message);
        void LogNotice(string message);
    }
}