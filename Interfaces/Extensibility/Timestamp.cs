﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Extensibility
{
    public static class Timestamp
    {
        public static string GetTimestamp() => DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
    }
}
