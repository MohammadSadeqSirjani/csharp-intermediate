﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Interfaces.Extensibility
{
    public class DbMigration
    {
        private readonly ILogger _logger;

        public DbMigration(ILogger logger)
        {
            _logger = logger;
        }

        public void Migrate()
        {
            MigrationStarted();

            if (TryMigrationProcessing()) MigrationCompleted();
            else MigrationCrashed();
        }

        private void MigrationStarted() => _logger.LogInfo($"Migration to the database is started at {DateTime.Now}");

        private void MigrationCompleted() => _logger.LogInfo($"Migrating to the database finished at {DateTime.Now}");
        private void MigrationCrashed() => _logger.LogError($"Migrating to the database crashed at {DateTime.Now}");

        private bool TryMigrationProcessing()
        {
            try
            {
                for (var i = 0; i <= 100; i++)
                {
                    if (i == 51) throw new InvalidOperationException();
                    _logger.LogNotice(($"Migrating to the database ... {i}%"));
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception.Message);
                return false;
            }

            return true;
        }


    }
}
