﻿using System;

namespace Interfaces.Polymorphism
{
    public class SmsNotificationChannel : INotificationChannel
    {
        public void Send(Message message) => Console.WriteLine("Sending SMS . . . ");
    }
}