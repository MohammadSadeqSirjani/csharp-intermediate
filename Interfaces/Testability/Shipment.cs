﻿using System;

namespace Interfaces.Testability
{
    public class Shipment
    {
        public float Cost { get; set; }
        public DateTime ShippingDate { get; set; }

        public override string ToString()
        {
            return $"[Cost: {Cost} - ShippingDate: {ShippingDate}]";
        }
    }
}