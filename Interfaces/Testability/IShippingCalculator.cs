﻿namespace Interfaces.Testability
{
    public interface IShippingCalculator
    {
        float CalculateShipping(Order order);
    }
}