﻿using System;

namespace Interfaces.Testability
{
    public class Order
    {
        public DateTime DatePlaced { get; set; }
        public float TotalPrice { get; set; }

        public bool IsShipped => Shipment != null;

        public Shipment Shipment { get; set; }

        public override string ToString()
        {
            return
                $"DatePlaced: {DatePlaced} - TotalPrice: {TotalPrice} - IsShipped: {IsShipped} - Shipment: {Shipment}";
        }
    }
}
