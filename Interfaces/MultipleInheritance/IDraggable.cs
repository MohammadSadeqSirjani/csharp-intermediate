﻿namespace Interfaces.MultipleInheritance
{
    public interface IDraggable
    {
        void Drag();
    }
}