﻿namespace Interfaces.MultipleInheritance
{
    public class TextBox : UiControl, IDraggable, IDroppable
    {
        public void Drag()
        {
        }

        public void Drop()
        {
        }
    }
}