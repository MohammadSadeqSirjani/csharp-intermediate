﻿namespace Interfaces.MultipleInheritance
{
    public interface IDroppable
    {
        void Drop();
    }
}