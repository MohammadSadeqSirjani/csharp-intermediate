﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inheritance
{
    public static class Enumerable<T> where T : new()
    {
        public static string WriteLine(IEnumerable<T> enumerable)
        {
            var stringBuilder = new StringBuilder(1024);
            var items = enumerable as T[] ?? enumerable.ToArray();
            var counter = items.Count();
            var global = "";

            foreach (var item in items)
            {
                stringBuilder.Append($"{item}, ");
                counter--;
                if (counter != 0) continue;
                var str = stringBuilder.ToString();
                global = $"[ {str.Remove(str.LastIndexOf(", ", StringComparison.Ordinal))} ]";
            }

            return global;
        }
    }
}