﻿using Inheritance.Casting;
using Inheritance.Constructor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Inheritance
{
    public static class Program
    {
        public static void Main(string[] args)
        {
        }

        private static void CustomStack()
        {
            var stack = new Exercises.Stack();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
            stack.Clear();
            Console.WriteLine(stack.Pop());
        }

        private static void Boxing()
        {
            var text = new Text();
            Shape shape = new Text();
            object obj = new Shape();

            // Convert from value type into reference type is boxing.
            // Convert from reference type into value type is unboxing.
        }

        private static void Casting()
        {
            var arrayList = new ArrayList()
            {
                1, "Mosh", new Text()
            };

            var integers = new List<int>()
            {
                Enumerable.Range(0, 10).Sum(),
                Enumerable.Range(0, 20).Sum(),
                Enumerable.Range(0, 30).Sum(),
                Enumerable.Range(0, 40).Sum(),
                Enumerable.Range(0, 50).Sum(),
                Enumerable.Range(0, 60).Sum(),
                Enumerable.Range(0, 70).Sum(),
                Enumerable.Range(0, 80).Sum(),
                Enumerable.Range(0, 90).Sum(),
                Enumerable.Range(0, 100).Sum()
            };

            var texts = new List<Text>()
            {
                new Text(),
                new Text(),
                new Text(),
                new Text(),
                new Text(),
                new Text(),
                new Text(),
                new Text(),
                new Text()
            };

            Console.WriteLine(Enumerable<int>.WriteLine(integers));
            Console.WriteLine(Enumerable<Text>.WriteLine(texts));
        }

        private static void Castigating()
        {
            // text object and shape object both reference to the same point of memory
            // but text has more items to use. it's happened because of casting. shape has lower 
            // item to show, they are different in views.
            var text = new Text();
            Shape shape = text;

            text.Width = 200;
            shape.Width = 100;

            Console.WriteLine(text.Width);
        }

        private static void Constructor()
        {
            var car1 = new Car();
            var car2 = new Car("AB12CD");
        }
    }
}
