﻿using System;

namespace Inheritance.Constructor
{
    public class Vehicle
    {
        private readonly string _registrationNumber;

        protected Vehicle()
        {
            Console.WriteLine("Vehicle is being initialized ... ");
        }

        protected Vehicle(string registrationNumber)
        {
            _registrationNumber = registrationNumber;
            Console.WriteLine($"Vehicle is being initialized ({registrationNumber}) ... ");
        }
    }
}