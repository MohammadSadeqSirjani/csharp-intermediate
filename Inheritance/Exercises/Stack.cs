﻿using System;
using System.Collections;

namespace Inheritance.Exercises
{
    public class Stack
    {
        private readonly ArrayList _arrayList = new ArrayList();

        private bool IsEmpty() => _arrayList.Count == 0;

        public void Push(object obj)
        {
            if(obj == null) throw new InvalidOperationException("Null value.");
            _arrayList.Add(obj);
        }

        public object Pop()
        {
            if(IsEmpty()) throw new InvalidOperationException("Stack empty.");
            var item = _arrayList[Count() - 1];
            _arrayList.RemoveAt(Count() - 1);
            return item;
        }

        public void Clear() => _arrayList.Clear();

        private int Count() => _arrayList.Count;
    }
}
