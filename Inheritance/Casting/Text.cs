﻿namespace Inheritance.Casting

{
    public class Text : Shape
    {
        public int FontSize { get; set; }
        public string FontName { get; set; }

        public override string ToString()
        {
            return "Text";
        }
    }
}