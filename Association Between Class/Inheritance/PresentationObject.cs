﻿using System;

namespace Association.Inheritance
{
    public class PresentationObject
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public void Copy()
        {
            Console.WriteLine("Object copied to the clipboard.");
        }

        public void Duplicate()
        {
            Console.WriteLine("Object was duplicated.");
        }
    }
}
