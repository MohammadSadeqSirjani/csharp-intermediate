﻿namespace Association.Composition
{
    public class DbMigratory
    {
        private readonly Logger _logger;

        public DbMigratory(Logger logger)
        {
            _logger = logger;
        }

        public void Migrate()
        {
            _logger.Log("We are migrating ... ");
        }
    }
}