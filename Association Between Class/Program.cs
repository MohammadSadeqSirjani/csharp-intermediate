﻿using Association.Composition;
using Association.Inheritance;

namespace Association
{
    public static class Program
    {
        public static void Main(string[] args)
        {
        }

        private static void Composition()
        {
            var logger = new Logger();

            var dbMigratory = new DbMigratory(logger);

            var installer = new Installer(logger);

            dbMigratory.Migrate();

            installer.Install();
        }

        private static void GetParent()
        {
            var text = new Text
            {
                Height = 100
            };

            text.Copy();
        }
    }
}
