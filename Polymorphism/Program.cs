﻿using Polymorphism.Exercises;
using Polymorphism.MethodOverriding;
using System.Collections.Generic;

namespace Polymorphism
{
    public static class Program
    {
        public static void Main(string[] args)
        {
        }

        private static void DbCommand()
        {
            using var connection = new OracleConnection("ConnectionString");
            var command = new DbCommand(connection, "SELECT * FROM tb_area");
            command.Execute();
        }

        private static void DbConnection()
        {
            using var sqlConnection = new SqlConnection("SqlConnection");
            sqlConnection.Open();

            using var oracleConnection = new OracleConnection("OracleConnection");
            oracleConnection.Open();
        }

        private static void Abstraction()
        {
            var shapes = new List<Abstraction.Shape>()
            {
                new Abstraction.Circle(),
                new Abstraction.Rectangle(),
                new Abstraction.Triangle(),
            };

            var canvas = new Abstraction.Canvas();
            canvas.DrawShape(shapes);

        }

        private static void MethodOverriding()
        {
            var shapes = new List<Shape>()
            {
                new Circle(),
                new Rectangle(),
                new Triangle(),
                new Shape()
            };

            var canvas = new Canvas();
            canvas.DrawShape(shapes);
        }
    }
}
