﻿using System;

namespace Polymorphism.Exercises
{
    public abstract class DbConnection
    {
        private readonly string _connectionString;

        protected static readonly TimeSpan Timeout = TimeSpan.FromMilliseconds(500);

        protected DbConnection(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException($"{nameof(connectionString)} is null or empty.");

            _connectionString = connectionString;
        }

        public abstract void Open();

        public abstract void Close();
    }
}
