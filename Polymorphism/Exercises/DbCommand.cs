﻿using System;

namespace Polymorphism.Exercises
{
    public class DbCommand
    {
        private readonly DbConnection _dbConnection;
        private readonly string _query;


        public DbCommand(DbConnection dbConnection, string query)
        {
            _dbConnection = dbConnection;
            if (string.IsNullOrEmpty(query)) throw new ArgumentNullException($"{nameof(query)} is null or empty.");
            _query = query;
        }

        public void Execute()
        {
            _dbConnection.Open();
            RunInstruction(_query);
            _dbConnection.Close();
        }

        private static void RunInstruction(string query) => Console.WriteLine($"Run this command: {query}");
    }
}
