﻿using System;
using System.Threading;

namespace Polymorphism.Exercises
{
    public class SqlConnection : DbConnection, IDisposable
    {
        public SqlConnection(string connectionString) : base(connectionString)
        {
        }

        public override void Open()
        {
            var start = DateTime.Now;
            Thread.Sleep(200);
            var timeout = DateTime.Now;

            if ((timeout - start).Duration().Milliseconds > Timeout.Milliseconds)
                throw new TimeoutException("Sql Connection timeout.");

            Console.WriteLine("Sql Connection is opened.");
        }

        public override void Close() => Console.WriteLine("Sql Connection is closed.");

        public void Dispose()
        {
            Close();
            Console.WriteLine("Sql Connection is disposed.");
        }
    }
}