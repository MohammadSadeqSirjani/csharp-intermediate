﻿using System;
using System.Threading;

namespace Polymorphism.Exercises
{
    public class OracleConnection : DbConnection, IDisposable
    {
        public OracleConnection(string connectionString) : base(connectionString)
        {
        }

        public override void Open()
        {
            var start = DateTime.Now;
            Thread.Sleep(250);
            var timeout = DateTime.Now;

            if ((timeout - start).Duration().Milliseconds > Timeout.Milliseconds)
                throw new TimeoutException("Oracle Connection timeout.");

            Console.WriteLine("Oracle Connection is opened.");
        }

        public override void Close() => Console.WriteLine("Oracle Connection is closed.");

        public void Dispose()
        {
            Close();
            Console.WriteLine("Oracle Connection is disposed.");
        }
    }
}