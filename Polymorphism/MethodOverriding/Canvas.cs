﻿using System.Collections.Generic;

namespace Polymorphism.MethodOverriding
{
    public class Canvas
    {
        public void DrawShape(IEnumerable<Shape> shapes)
        {
            foreach (var shape in shapes) shape.Draw();
        }
    }
}