﻿using System;

namespace Polymorphism.MethodOverriding
{
    public class Circle : Shape
    {
        public override void Draw() => Console.WriteLine("Draw a circle.");
    }
}
