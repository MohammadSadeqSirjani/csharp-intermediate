﻿using System;

namespace Polymorphism.MethodOverriding
{
    public class Triangle : Shape
    {
        public override void Draw() => Console.WriteLine("Draw a triangle.");
    }
}