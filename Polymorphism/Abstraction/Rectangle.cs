﻿using System;

namespace Polymorphism.Abstraction
{
    public class Rectangle : Shape
    {
        public override void Draw() => Console.WriteLine("Draw a rectangle.");
    }
}