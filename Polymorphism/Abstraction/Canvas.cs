﻿using System.Collections.Generic;

namespace Polymorphism.Abstraction
{
    public class Canvas
    {
        public void DrawShape(IEnumerable<Shape> shapes)
        {
            foreach (var shape in shapes) shape.Draw();
        }
    }
}