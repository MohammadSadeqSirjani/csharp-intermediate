﻿using Classes.Exercises;
using Classes.Indexer;
using Classes.Methods;
using System;
using System.Threading;

namespace Classes
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            GetPost();
        }

        private static void GetPost()
        {
            var post = new Post()
            {
                Title = "Post Title",
                Description = "Post Description"
            };

            post.UpVote();

            Console.WriteLine(post);
            post.UpVote();

            Console.WriteLine(post);

            post.UpVote();
            post.UpVote();

            Console.WriteLine(post);

            post.DownVote();
            post.DownVote();

            Console.WriteLine(post);


            post.DownVote();

            Console.WriteLine(post);
        }

        private static void Duration()
        {
            var stopWatch = new Exercises.StopWatch();
            stopWatch.Start();
            Thread.Sleep(5000);
            stopWatch.Stop();
            Console.WriteLine(stopWatch.Elapsed());
        }

        private static void Indexer()
        {
            var cookie = new HttpCookie
            {
                ["name"] = "Mosh"
            };

            Console.WriteLine(cookie["name"]);
        }

        private static void Encapsulation()
        {
            var person = new Properties.Person(new DateTime(1971, 4, 27));

            Console.WriteLine(person.Age);

            person.ChangeBirthDate(new DateTime(2000, 4, 27));

            Console.WriteLine(person.Age);
        }

        private static void AccessModifier()
        {
            var customer = new Fields.Customer(1);
            customer.Orders.Add(new Fields.Order());
            customer.Orders.Add(new Fields.Order());

            Console.WriteLine(customer.Orders.Count);

            customer.Promote();

            Console.WriteLine(customer.Orders.Count);
        }

        private static void UseOut()
        {
            if (int.TryParse("abc", out var number)) Console.WriteLine(number);
        }

        private static void UseParams()
        {
            var calculator = new Calculator();

            Console.WriteLine(calculator.Add(1, 2, 3));
            Console.WriteLine(calculator.Add(1, 2, 3, 4));
            Console.WriteLine(calculator.Add(1, 2, 3, 4, 5));
            Console.WriteLine(calculator.Add(new int[] { 1, 2, 3, 4, 5 }));
        }

        private static void UsePoints()
        {
            try
            {
                MovePoint();
            }
            catch (Exception exception)
            {
                Console.WriteLine(new
                {
                    Message = $"An error occured with ana apology ... ({exception.Message})"
                });
            }
        }

        private static void MovePoint()
        {
            var point = new Point()
            {
                X = 10,
                Y = 20
            };

            point.Move(20, 10);

            Console.WriteLine(point);

            point.Move(null);

            Console.WriteLine(point);
        }

        private static void ObjectInitializer()
        {
            var mosh = new Student()
            {
                Id = 1,
                Forename = "Mosh",
                Surname = "Hamedani",
                BirthDate = new DateTime(1970, 10, 25)
            };
        }

        private static void Constructor()
        {
            var john = new Customer(1, "John");
            Console.WriteLine($"{john.Id} - {john.Name}");
        }

        private static void Introduction()
        {
            var mosh = new Person()
            {
                Name = "Mosh"
            };

            mosh.Introduce("John");

            var john = Person.Parse("John");
            john.Introduce("Mosh");
        }
    }
}
