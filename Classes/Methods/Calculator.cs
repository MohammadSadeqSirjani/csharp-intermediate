﻿using System.Linq;

namespace Classes.Methods
{
    public class Calculator
    {
        public int Add(params int[] numbers)
        {
            return numbers.Sum();
        }

    }
}
