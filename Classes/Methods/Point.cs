﻿namespace Classes.Methods
{
    public class Point
    {
        public decimal X { get; set; }

        public decimal Y { get; set; }

        public void Move(decimal x, decimal y)
        {
            X = x;
            Y = y;
        }

        public void Move(Point location)
        {
            if (location == null) return;
            Move(location.X, location.Y);
        }

        public override string ToString()
        {
            return $"X : {X}, Y : {Y}";
        }
    }
}
