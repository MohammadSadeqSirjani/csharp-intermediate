﻿using System;

namespace Classes.Access_Modifier
{
    public class Person
    {
        private string _name;

        public DateTime BirthDate { get; set; }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value)) return;
                _name = value;
            }
        }
    }
}
