﻿using System;

namespace Classes.Properties
{
    public class Person
    {
        public const int NumberOfDaysOfYear = 365;
        public DateTime BirthDate { get; private set; }
        public int Age => (DateTime.Today - BirthDate).Days / NumberOfDaysOfYear;

        public Person(DateTime birthDate)
        {
            BirthDate = birthDate;
        }

        public void ChangeBirthDate(DateTime birthDate)
        {
            BirthDate = birthDate;
        }
    }
}