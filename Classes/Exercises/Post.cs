﻿using System;

namespace Classes.Exercises
{
    public class Post
    {
        public string Title { get; set; }

        public string Description { get; set; }

        private readonly DateTime _createDateTime;

        private int _vote;

        public Post()
        {
            _createDateTime = DateTime.Now;
            _vote = 0;
        }

        public void UpVote()
        {
            _vote++;
        }

        public void DownVote()
        {
            _vote--;
        }

        public override string ToString()
        {
            return $"Title: {Title}, Description: {Description}, CreateDate: {_createDateTime}, Vote: {_vote}";
        }
    }
}
