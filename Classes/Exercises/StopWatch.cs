﻿using System;

namespace Classes.Exercises
{
    public class StopWatch
    {
        private static DateTime _start;
        private static bool _isRunning;
        private static DateTime _stop;


        public void Start()
        {
            if (_isRunning) throw new InvalidOperationException(nameof(_isRunning));
            _start = DateTime.Now;
            _isRunning = true;
        }

        public void Stop()
        {
            if (!_isRunning) throw new InvalidOperationException(nameof(_isRunning));
            _stop = DateTime.Now;
            _isRunning = false;
        }

        public TimeSpan Elapsed()
        {
            return (_stop - _start).Duration();
        }
    }
}
