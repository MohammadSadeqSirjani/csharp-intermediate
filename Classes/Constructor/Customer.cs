﻿using System.Collections.Generic;

namespace Classes
{
    public class Customer
    {
        public int Id { get; }

        public string Name { get; }

        public IList<Order> Orders { get; }

        public Customer()
        {
            Orders = new List<Order>();
        }

        public Customer(int id) : this()
        {
            Id = id;
        }

        public Customer(int id, string name) : this(id)
        {
            Name = name;
        }
    }
}