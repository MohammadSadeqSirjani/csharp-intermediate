﻿using System.Collections.Generic;

namespace Classes.Fields
{
    public class Customer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public readonly IList<Order> Orders = new List<Order>();

        public Customer(int id)
        {
            Id = id;
        }

        public Customer(int id, string name) : this(id)
        {
            Name = name;
        }

        public void Promote()
        {
            //Orders = new List<Order>();
        }
    }
}
