﻿using System;

namespace Classes
{
    public class Student
    {
        public int Id { get; set; }

        public string Forename { get; set; }

        public string Surname { get; set; }

        public DateTime BirthDate { get; set; }
    }
}
